all: bonnes_pratiques_PSMN.pdf

bonnes_pratiques_PSMN.pdf: bonnes_pratiques_PSMN.tex
	lualatex  bonnes_pratiques_PSMN.tex

clean:
	rm bonnes_pratiques_PSMN.{aux,bcf,dvi,log,nav,out,run.xml,snm,toc,pdf}
